
function isGameOver(board) {
    // console.log("reached here 1")
    for (row = 0; row <= 4; row++) {
        if (board[row] > 0) {
            // console.log("Game not Over")
            return false
        }
    }
    // console.log("Game Over")
    return true 
}
    
function minimax(board, isMyTurn, depth) {
    // sleep(1000);
    // console.log("========================================" );
    // console.log();
    // console.log("reached hereq");
    if (isGameOver(board) == true) {
        if (isMyTurn == true){
            return -1
        } else {
            return 1
        }
    } 
    // console.log("reached here 2")
    if (isMyTurn == false){
        var bestScore = Infinity;
    } else {
        var bestScore = -Infinity;
    }
    // console.log("reached here 3")
    
    for (var row = 0; row <= 4; row++) {
        // if (board[row] < 0) return -1;
        if (board[row] > 0) {
            var newBoard = [...board];
            while (newBoard[row] > 0) {
                newBoard[row] = newBoard[row] - 1
                // console.log("new board row: " + row + " value: "+ newBoard[row])
                if (isMyTurn == true){
                    score = minimax(newBoard, false, depth+1);
                    // console.log("minmax:: isMyTurn:" + isMyTurn + " depth: " + depth +  "score: "+ score +" " + board) ;
                    // if (score > bestScore) console.log("new best score: "+ score)
                    bestScore = Math.max(bestScore, score)
                } else {
                    score = minimax(newBoard, true, depth + 1);
                    // console.log("minmax:: isMyTurn:" + isMyTurn + " depth: " + depth +  "score: "+ score +" " + board) ;
                    // if (score < bestScore) console.log("new best score: "+ score)
                    bestScore = Math.min(bestScore, score);
                }
            }
        }
    }
    return bestScore;
}

function old_minimax(board, myMark, isMyTurn, depth) {
    // console.log("entered minimax, depth: " + depth)
    // console.log(...board);
    if (myMark == "X") {
        oppMark = "O";
    } else {
        oppMark = "X";
    }
    console.log("checking if game Over")
    console.log(...board)
    if (isGameOver(board) == true) {
        if (isMyTurn == true){
            return 1
        } else {
            return -1
        }
    } else {
        // game still on
        // console.log("Game still on")
        if (isMyTurn == false){
            var bestScore = Infinity;
        } else {
            var bestScore = -Infinity;
        }
        for (var row = 0; row <= 4; row++) {
            for (var col = 0; col <= row; col++) {
                // console.log("in loop:  should be same as above::");
                // console.log(board);
                // console.log(row + ": " + board[row]);
                // console.log("Next :: check value >>" + [...board] + "<<");
                if (board[row][col] == "") {
                    var newBoard = [
                        [...board[0]],
                        [...board[1]],
                        [...board[2]],
                        [...board[3]],
                        [...board[4]]
                    ];
                    
                    if (isMyTurn == true){
                        newBoard[row][col] = myMark;
                        // console.log("simulating AI next move");
                        score = minimax(newBoard, myMark, false, depth+1);
                        // console.log("got a score back. score: " + score 
                        //     + " depth: " + depth
                        //     + " from depth: " + (depth + 1)
                        // );
                        bestScore = Math.max(bestScore, score)
                    } else {
                        newBoard[row][col] = oppMark;
                        // console.log("simulating humans next move");
                        score = minimax(newBoard, myMark, true, depth + 1);
                        // console.log("got a score back. score: " + score
                        //     + " depth: " + depth
                        //     + " from depth: " + (depth + 1)
                        // );
                        bestScore = Math.min(bestScore, score);
                    }
                    // console.log("if clause finished")
                }
                // console.log("loop finished")
            }
        }
        return bestScore;
    }
}

function impossibleAI(board) {
    // alert(board)
    console.log("entered impossible AI")
    var bestScore = -Infinity;
    // console.log(...board);
    var flatBoard = []
    for (var row = 0; row <= 4; row++) {
        flatBoard[row] = board[row].filter(x => x=="").length;
    }
    // early quick play
    if (flatBoard.reduce((a, b) => a + b, 0) == 150) {
        bestMove = [0,1]
        bestScore = 0
    } else {

        console.log("flatboard: " + flatBoard)
        
        for (var row = 0; row <= 4; row++) {
            if (flatBoard[row] > 0) {
                console.log("Found row with slots: " + row + ", slots: " + flatBoard[row])
                var newBoard = [...flatBoard];
                while (newBoard[row] > 0) {
                    console.log("simulating row: " + row + ", value: " + newBoard[row])
                    newBoard[row] = newBoard[row] - 1
                    score = minimax(newBoard, false, 1);
                    if (score >= bestScore) {
                        bestMove = [row, flatBoard[row] - newBoard[row] ];
                        bestScore = score;
                        console.log("best Score: " + bestScore + " bestMove: " + bestMove)
                    }
                }
            }
        }
    }
    console.log("returned best move: "+ bestMove + " with bestScore: " + bestScore)
    // alert(board)
    // PlayTurn
    cnt = bestMove[1]
    row = bestMove[0]
    for (col = 0; cnt > 0 && col <= row; col++) {
        // console.log("inside cnt loop: " + cnt)
        if (board[row][col] == "") {
            console.log("Computer plays:: row: " + row + ", col:" + col)
            playTurn(row,col)
            cnt--
        }
    }
    console.log("Leave ImpossibleAI")
    return switchTurn()
}