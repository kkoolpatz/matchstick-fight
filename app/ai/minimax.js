


function isGameOver(board) {
    for (row = 0; row < 3; row++) {
        if (board[row][0] == board[row][1]
            && board[row][2] == board[row][1]
            && board[row][2] != "") {
            return board[row][0];
        } else if (board[0][row] == board[1][row]
            && board[2][row] == board[1][row]
            && board[2][row] != "") {
            return board[0][row];
        }
    }
    if (board[0][0] == board[1][1]
        && board[1][1] == board[2][2]
        && board[1][1] != "") {
        // console.log("game over");
        return board[0][0];
    } else if (board[0][2] == board[1][1]
        && board[1][1] == board[2][0]
        && board[1][1] != "") {
        // console.log("game over");
        return board[0][2];
    } else if ([...board[0], ...board[1], ...board[2]].includes("")) {
        // console.log("game not over")
        return null; // Game in progress
    } else {
        // console.log("game tied")
        return "-";  // Game tied
    }
}

function minimax(board, myMark, isMyTurn, depth) {
    // console.log("entered minimax, depth: " + depth)
    // console.log(...board);
    if (myMark == "X") {
        oppMark = "O";
    } else {
        oppMark = "X";
    }
    // console.log("checking if game Over")
    result = isGameOver(board);
    if (result == myMark) {
        // console.log("returned 1")
        return 1;
        // console.log("still here")
    } else if (result == "-") {
        // console.log("returned 0")
        return 0;
    } else if (result != null) {
        // console.log("returned -1")
        return -1;
    } else {
        // game still on
        // console.log("Game still on")
        if (isMyTurn == false){
            var bestScore = Infinity;
        } else {
            var bestScore = -Infinity;
        }
        for (var row = 0; row < 3; row++) {
            for (var col = 0; col < 3; col++) {
                // console.log("in loop:  should be same as above::");
                // console.log(board);
                // console.log(row + ": " + board[row]);
                // console.log("Next :: check value >>" + [...board] + "<<");
                if (board[row][col] == "") {
                    var newBoard = [
                        [...board[0]],
                        [...board[1]],
                        [...board[2]]
                    ];
                    
                    if (isMyTurn == true){
                        newBoard[row][col] = myMark;
                        // console.log("simulating AI next move");
                        score = minimax(newBoard, myMark, false, depth+1);
                        // console.log("got a score back. score: " + score 
                        //     + " depth: " + depth
                        //     + " from depth: " + (depth + 1)
                        // );
                        bestScore = Math.max(bestScore, score)
                    } else {
                        newBoard[row][col] = oppMark;
                        // console.log("simulating humans next move");
                        score = minimax(newBoard, myMark, true, depth + 1);
                        // console.log("got a score back. score: " + score
                        //     + " depth: " + depth
                        //     + " from depth: " + (depth + 1)
                        // );
                        bestScore = Math.min(bestScore, score);
                    }
                    // console.log("if clause finished")
                }
                // console.log("loop finished")
            }
        }
        return bestScore;
    }
}

function impossibleAI(board, myMark) {
    // alert(board)
    var bestScore = -Infinity;
    // console.log("entered impossible AI")
    // console.log(...board);

    for (var row = 0; row < 3; row++) {
        for (var col = 0; col < 3; col++) {
            if (board[row][col] == "") {
                var newBoard = [ 
                    [...board[0]],
                    [...board[1]],
                    [...board[2]]
                ];
                newBoard[row][col] = myMark; 
                // alert(newBoard + "\n" + gameGrid)               
                score = minimax(newBoard, myMark, false, 1);
                // console.log(" 0 level score: "+ score)
                if (score > bestScore) {
                    bestScore = score;
                    // console.log(row + ", "+ col)
                    bestMove = [row, col];
                }
            }
        }
    }
    // console.log("returned: "+ bestMove + " with bestScore: " + bestScore)
    // alert(board)
    return bestMove;
}