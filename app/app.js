

// Global variables begin here 

var canvas = document.getElementById("myCanvas");
var changeMarkButton = document.getElementById("changeMark");
var changePlayOrderButton = document.getElementById("changePlayOrder");
var ctx = canvas.getContext("2d");
canvas.width = Math.min(window.innerHeight, window.innerWidth) * 0.7;
canvas.height = canvas.width + 50;

var mouseX;
var mouseY;

// In-Game global Variables begin here

var gameGrid;
var gameMode = "HC";
var gridSize = Math.min(canvas.height, canvas.width);
var cellSize = gridSize / 5;
var unitSize = Number(cellSize / 4);
// console.log("global unitsize: " + unitSize);
var isHumanTurn = true;
var isX = true;
var gameRestart;
var currentRow = -1;
var played = false

// Initial state of the Game
var matchstick=new Image();
matchstick.src = 'img/ms.png';
matchstick.onload=init;
// init();
function init() {
    
    
    gameGrid = [
        [""],
        ["", ""],
        ["", "", "",],
        ["", "", "", ""],
        ["", "", "", "", ""]
    ];
    
    console.log("isHumanTurn: " + isHumanTurn + ", isX : " + isX);
    // gameGrid = [
    //     ["X", "O", "O"],
    //     ["O" , "", "" ],
    //     ["" , "X", "X"]
    // ];

    gameRestart = false;
    currentRow = -1;
    drawCanvas();
    setFirstPlayer();
    setMark();
}

// Event listeners

canvas.addEventListener("mouseup", mouseUp, false);
canvas.addEventListener("mousemove", mouseMove, false);
window.addEventListener('resize', drawCanvas, false);


function setMark() {
    if (isHumanTurn == true && changeMarkButton.innerHTML[8] == "O") {
        isX = true
        console.log("human will play as X");
    } else if (isHumanTurn == false && changeMarkButton.innerHTML[8] == "O") {
        isX = false
        console.log("computer plays first, as O");
    } else if (isHumanTurn == false && changeMarkButton.innerHTML[8] == "X") {
        isX = true
        console.log("computer plays first, as X");
    } else if (isHumanTurn == true && changeMarkButton.innerHTML[8] == "X") {
        isX = false
        console.log("human plays first, as O");
    }
}


function changeMark() {
    isX = true
    console.log("Game Is Reset");
    changeMarkButton.innerHTML = "Reset";
    init();
}


function setFirstPlayer(){
    if (changePlayOrderButton.innerHTML[5] == "S") {
        console.log("Human Plays first");
        isHumanTurn = true;
        drawResult("You go first!")
    } else {
        console.log("Computer Plays first");
        isHumanTurn = false;
    }
}



function changePlayOrder() {
    var changePlayOrderButton = document.getElementById("changePlayOrder");
    if (changePlayOrderButton.innerHTML[5] == "S") {
        console.log("Computer Plays first");
        isHumanTurn = false;
        changePlayOrderButton.innerHTML = "Play First";
    } else {
        console.log("Human Plays first");
        isHumanTurn = true;
        changePlayOrderButton.innerHTML = "Play Second";
    }
    init();
}

// checks if game has ended 

function hasGameEnded() {
    console.log("Game Restart is " + gameRestart)
    if ([...gameGrid[0], ...gameGrid[1], ...gameGrid[2], ...gameGrid[3], ...gameGrid[4]].includes("")) {
        gameRestart = false
        return false
    } else { 
        gameRestart = true
        return true }
}


// Plays the next turn for all players

function switchTurn(){
    console.log("Switch Turn Called");
    if(played ==  false) {
        drawResult("You must play before you complete your turn");
        return
    }
    
    currentRow = -1;
    if (hasGameEnded() == true) {
        console.log("Game is over");
        if (gameMode == "HC"){
            //Move to Turn Complete
            if (isHumanTurn == true) {
                drawResult("You Win, Click Game to Restart");
            } else {
                drawResult("You Lost, Click Game to Restart");
            }
        }
    } else {
        console.log("Game is not over yet");
        if (isX === true) {
            isX = false;
        } else {
            isX = true;
        }
        if (gameMode == "HC"){
            if (isHumanTurn == true) {
                console.log("switch turn: Human -> Computer");
                played =  false
                drawResult("Computer is thinking...");
                sleep(200);
                isHumanTurn = false;
            } else {
                console.log("switch turn: Computer -> Human");
                played =  false
                drawResult("Your turn...");
                isHumanTurn = true;
            }
        }
    }
    console.log("Switch Turn function end");
}

function playTurn(row, col){
    if (row > 4 || col > 4) {
        console.log("invalid playturn: " + row + ", " + col);    
        return
    }
    if (currentRow == -1 || row == currentRow) {

        console.log("playturn: "+ row +", "+ col);
        if (gameGrid[row][col] == "") {
            // console.log("valid move");
            if (isX === true) {
                // console.log("marking X" + row + ", " + col);
                drawX(row, col)
                // isX = false; //Move to Turn Complete
            } else {
                drawO(row, col)
                // console.log("marking O" + row + ", " + col);
                // isX = true; //Move to Turn Complete
            }
            if (currentRow == -1) currentRow = row;
            played = true
        } else {
            console.log("Invalid move: gameGrid[row][col]: " + gameGrid);
            
        }
    } else {
        drawResult("Error: You must pick a candle from the same group")
    }
    
}

function autoplay(){
    if (gameRestart == true){
        console.log("Game Restart is " + gameRestart)
        return
    } else if (isX == true) {
        var myMark = "X";
    } else 
        var myMark = "O";
    console.log("autoplay returns control")
    return impossibleAI(...[gameGrid])
}


function mouseMove(e) {
    mouseX = e.pageX - canvas.offsetLeft;
    mouseY = e.pageY - canvas.offsetTop;
    // ctx.font = "10px Arial";
    // wipeCanvas(340, 0, canvas.width, canvas.height)
    // ctx.fillStyle = "black";
    // ctx.fillText(mouseX + " : " + mouseY, 350, 170);
    
}

function mouseUp(e) {
    console.log("Mouse up");
    mouseX = e.pageX - canvas.offsetLeft;
    mouseY = e.pageY - canvas.offsetTop;
    if (mouseX > gridSize 
        || mouseY > gridSize
        || mouseX < 0
        || mouseY < 0) {
        return
    }

    console.log("MouseX: " + mouseX);
    console.log("MouseY: " + mouseY);
    if (gameRestart == true) {
        init()
        console.log("Mouse reported game restarted");
        return
    }   
    // console.log("Mouse reported game ongoing");

    col = Math.trunc(mouseX / cellSize)
    row = Math.trunc(mouseY / cellSize)
    
    if (isHumanTurn === true)
        playTurn(row, col);
}

function draw() {
    // console.log("Inside Draw");
    if (isHumanTurn === false) {
        console.log("Inside Draw:: Not Human turn");
        autoplay()
        console.log("after auto play");
    }
}
setInterval(draw, 10);


